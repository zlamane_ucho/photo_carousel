$("#left").on('click', shiftArrayLeft);
$("#right").on('click', shiftArrayRight);

let activePhoto = 2;
}
let $carousel = $(".carousel-item");
let classes = ["disabled-left", "fade-out-left", "active", "fade-out-right", "disabled-right"];

$carousel.each((index, el) => {
    $(el).addClass(classes[index]);
});

function shiftArrayLeft() {
    let $poped = $carousel.splice(0,1);
    $carousel = [...$carousel,...$poped];
    addClasses($carousel);
};

function shiftArrayRight() {
    let $poped = $carousel.splice($carousel.length - 1,1);
    $carousel = [...$poped, ...$carousel];
    addClasses($carousel);
};

function addClasses(carousel) {
    $(carousel).each((index, el) => {
        $(el).removeClass().addClass("carousel-item").addClass(classes[index]);
    });